const getSum = (str1, str2) => {
  if(typeof str1 !='string' && typeof str2 !='string'){
    return false;
  }
  
  let sum = (+str1) + (+str2)
  return sum.toString();
}
 

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
   let post_counter = 0;
   let comment_counter = 0;
   for(let i = 0; i<listOfPosts.lenght; i++){
    
    if(listOfPosts[i].author == authorName){
      post_counter++;  
    }
    if(listOfPosts[i].comments != 0 && comment.author == authorName){
        comment_counter++;
    }
   }
   return "Post:" + post_counter + ",comments:" +comment_counter;   
};

const tickets=(people)=> {
   let sum = 0;
   for(const m of people){
    if((m - 25) > sum) return 'NO';
    sum +=25;
   }
   return 'YES'
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
